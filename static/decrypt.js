function xorDecrypt(data, key) {
    const decryptedData = new Uint8Array(data.length);
    for (let i = 0; i < data.length; i++) {
        decryptedData[i] = data[i] ^ key;
    }
    return decryptedData;
}

fetch('/static/BefiYesharim.123')  // Replace with the correct path
    .then(response => response.arrayBuffer())
    .then(encryptedData => {
        const encryptionKey = 123;  // Use the same encryption key you used in Python
        const decryptedData = xorDecrypt(new Uint8Array(encryptedData), encryptionKey);

        // Create a Blob with the decrypted data
        const decryptedBlob = new Blob([decryptedData], { type: 'font/opentype' });

        // Create a CSS @font-face rule to use the decrypted font
        const fontUrl = URL.createObjectURL(decryptedBlob);
        const style = document.createElement('style');
        style.textContent = `
            @font-face {
                font-family: 'CustomFont';
                src: url(${fontUrl}) format('opentype');
            }
        `;

        // Apply the font to the page
        document.head.appendChild(style);
        document.body.style.fontFamily = 'CustomFont, sans-serif';
    })
    .catch(error => console.error('Error fetching and decrypting font:', error));
