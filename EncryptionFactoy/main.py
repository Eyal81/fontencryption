import sys

def simple_xor_encrypt_decrypt(input_file, output_file, key):
    with open(input_file, 'rb') as file_in, open(output_file, 'wb') as file_out:
        while True:
            byte = file_in.read(1)
            if not byte:
                break
            encrypted_byte = ord(byte) ^ key
            file_out.write(bytes([encrypted_byte]))

def main():
    if len(sys.argv) != 4:
        print("Usage: python encrypt.py <input_file> <output_file> <encryption_key>")
        sys.exit(1)

    input_file = sys.argv[1]
    output_file = sys.argv[2]
    encryption_key = int(sys.argv[3])

    simple_xor_encrypt_decrypt(input_file, output_file, encryption_key)
    print(f"File '{input_file}' encrypted and saved to '{output_file}' with key {encryption_key}.")

if __name__ == "__main__":
    main()
